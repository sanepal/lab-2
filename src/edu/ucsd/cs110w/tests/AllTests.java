package edu.ucsd.cs110w.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite(AllTests.class.getName());
        //$JUnit-BEGIN$
        suite.addTestSuite(CelsiusTests.class);
        suite.addTestSuite(FahrenheitTests.class);
        suite.addTestSuite(KelvinTests.class);
        //$JUnit-END$
        return suite;
    }

}


package edu.ucsd.cs110w.temperature; 
public class Celsius extends Temperature 
{ 
    public Celsius(float t)
    {
        super(t); 
    } 
    public String toString() 
    { 
        return "" + this.getValue() + " C";
    }
    @Override
    public Temperature toCelsius() {
        return this;
    }
    @Override
    public Temperature toFahrenheit() {
        return new Fahrenheit(this.getValue()*1.8f + 32);
    }
    @Override
    public Temperature toKelvin() {
        return new Kelvin(this.getValue()+273.15f);
    } 
} 

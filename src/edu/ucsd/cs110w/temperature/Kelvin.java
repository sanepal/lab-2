/**
 * 
 */
package edu.ucsd.cs110w.temperature;

public class Kelvin extends Temperature{
    public Kelvin(float t) 
    { 
        super(t); 
    } 

    public String toString() 
    { 
        return "" + this.getValue() + " K";
    } 
    @Override 
    public Temperature toCelsius() { 
        return new Celsius(this.getValue() - 273.15f);
    } 
    @Override 
    public Temperature toFahrenheit() { 
        return new Fahrenheit((this.getValue()-273.15f)*1.8f + 32);
    }

    @Override
    public Temperature toKelvin() {
        return this;
    } 


}
